let lg,
    isInit;

/**
 *
 * @param {string} body
 * @memberOf helper
 * @param {Object<string, string>}headers
 */
function parseBody(body, headers) {
    if (typeof body !== "string") {
        throw new Error('Body for parsing most be "string"');
    }
    let payload;
    const contentType = headers['content-type'];
    if (contentType.indexOf('application/json') >= 0) {
        payload = JSON.parse(body);
    } else {
        throw new Error('Unsupported content type of response body. Content type is: ' + contentType);
    }
    return payload;
}


/**
 * callback for handling unknown error
 * @memberOf helper
 * @param err
 */
function errHandler(err) {
    if (err) {
        lg.error(`UNKNOWN ERROR: ${err.code} ${err.message}`);
    }
}


/**
 * It parsed data from incoming request to server
 * @memberOf helper
 * @param {http.ClientRequest} req
 * @param {Object} data
 * @param {Bot~Callback} cb
 */
function getData(req, data, cb) {
    lg.silly(`Start Helper.getData()`);
    let body = [], payload;
    req.on('response', (res) => {
        const {statusCode, headers} = res;
        lg.silly(`Status Code is: ${statusCode}`);
        res.setEncoding('utf-8');
        res.on('data', (chunk) => {
            body.push(chunk);
        });
        res.on('end', () => {
            payload = parseBody(body.join(''), headers);
            lg.silly(`Payload is: \n${JSON.stringify(payload)}`);
            cb(null, {payload, headers, statusCode});
        });
        res.on('error', (err) => {
            cb(err);
        });
    });
    req.on('timeout', () => {
        const err = new Error(`Timout of connection, I can't get response from remote server`);
        cb(err);
    }).on('socket', socket => {
        socket.on('error', e => {
            cb(e);
        });
    });
    req.write(data);
    req.end();
}

/**
 *
 * @memberOf helper
 * @param {Error} err
 * @return {string}
 */
function errToString(err) {
    if (typeof err === 'string') return err;
    return `Code: ${err.code}, msg: ${err.message}, trace: ${err.trace}`;
}

/**
 *
 * Объкт возвращаемый на неправильный вебхук от "Мой Склад"
 * Возвращается в том случае если запрос идет не запросом
 * "POST" и не содержит ожидаемый контент
 *
 * @memberOf helper
 * @return {{error: {code: number, description: string}}}
 */
function getBadRequestObject() {
    return {error: {code: 400, description: 'Payload has bad format'}};
}

/**
 *
 * Объект возвращаемый в ответ на правильный вебхук
 * от "Мой Склад"
 *
 * @memberOf helper
 * @return {{error: {code: number, description: string}}}
 */
function getSuccessRequestObject() {
    return {error: {code: 200, description: 'OK'}};
}


/**
 * Определеяет окружение по переменномй окружения NODE_EVN
 * @memberOf helper
 * @return {boolean}
 */
function isDev() {
    return process.env.NODE_ENV === 'development';
}


/**
 * Определеяет окружение по переменномй окружения NODE_EVN
 * @memberOf helper
 * @memberOf helper
 * @return {boolean}
 */
function isProd() {
    return process.env.NODE_ENV === 'production';
}

/**
 *
 * @memberOf helper
 * @alias helper
 */
const helper = {
    getBadRequestObject, errHandler,
    getData, errToString, getSuccessRequestObject,
    isProd, isDev
};

/**
 *
 * @param {Object} config
 * @param {Logger} config.lg
 * @param {Object} config.ssl
 * @param {string} config.ssl.key
 * @param {string} config.ssl.cert
 * @param {string} config.ssl.chain
 * @param {string} config.ssl.full_chain
 */
module.exports = function (config) {
    if (!isInit) {
        lg = config.lg;
        isInit = true;
    }
    return helper;
};

// * @return {{isProd: (function(): boolean), isDev: (function(): boolean), errToString: errToString, getSuccessRequestObject: (function(): {error: {code: number, description: string}}), getBadRequestObject: (function(): {error: {code: number, description: string}}), errHandler: errHandler, getKeyAndCertificate: getKeyAndCertificate, getListValidNamesOfStates: (function(): string[]), getData: getData }}













